# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 15:22:13 2018

@author: rahuln
"""
import re
import os
from settings import pdf_img_conv
from settings import filters
from settings import form0_segmentation_cordinates
from settings import form1_segmentation_cordinates
import glob

file_path=''
file_name=''
seg_flag=''
filter_map=''
form_type=''
page_numbers=''
folder_postfix=''
segment_folder_postfix='_segmented'

filtered_image_postfix='_filtered'
pdf_img_param=''
filter_param=''
form_extracted_image=''

def setParameters(file_path_input,seg_flag_input,filter_map_input,form_type_input,page_numbers_input):
    global file_path
    file_path=file_path_input
    global file_name
    file_name=file_path.split('\\')[-1]
    #remove file name from path
    
    file_path=re.sub(file_name, '', file_path)  
    global seg_flag 
    seg_flag=seg_flag_input
    global filter_map 
    filter_map=filter_map_input
    global form_type
    form_type=form_type_input
    global page_numbers
    page_numbers=page_numbers_input           
    
    
def create_dir(file_path,file_name):
    import datetime
    global datetime_postfix
    datetime_postfix = datetime.datetime.now()
    global folder_postfix
    folder_postfix=datetime_postfix.now().strftime('%Y-%m-%d_%H-%M-%S')
    
    if not os.path.exists(file_path+file_name+folder_postfix):
        os.makedirs(file_path+file_name+folder_postfix)

    form_extracted_image_temp=conv_pdf_img(file_path,file_name)
    
    for fn in form_extracted_image_temp:        
        if not os.path.exists(file_path+file_name+folder_postfix+'\\'+fn+segment_folder_postfix):
            os.makedirs(file_path+file_name+folder_postfix+'\\'+fn+segment_folder_postfix)


    
def conv_pdf_img(file_path,file_name):
    global pdf_img_param
    pdf_img_param=' '
    
    for key,value in pdf_img_conv.items():
        pdf_img_param=pdf_img_param+' -'+str(key)+' '+str(value)
        
    os.system('magick '+pdf_img_param+' '+file_path+file_name+' '+file_path+file_name+folder_postfix+'\\'+file_name.split('.')[0]+'.jpg')
    
    global form_extracted_image
    form_extracted_image=sorted(os.listdir(file_path+file_name+folder_postfix))   
    return form_extracted_image


def apply_filters(filepath,file_name,filter_map,form_type,page_numbers):
    global filter_param
    filter_list=filter_map.split(',')
    
    for fil in filter_list:
        value=filters[fil]
        filter_param=filter_param+' '+str(value)
        
    for pages in page_numbers:
        applicable_image=form_extracted_image[pages]
        command='magick convert '+file_path+file_name+folder_postfix+'\\'+applicable_image+' '+filter_param+' '+file_path+file_name+folder_postfix+'\\'+applicable_image.split('.')[0]+filtered_image_postfix+'.jpg'
        os.system(command)
        #print(command)
    
    return glob.glob(file_path+file_name+folder_postfix+'\\*'+filtered_image_postfix+'*')


def crop_image(image,top_x,top_y,bottom_x,bottom_y,postfix):   
    from PIL import Image
    img = Image.open(image)
    img2 = img.crop((top_x,top_y,bottom_x,bottom_y))
    img2.save(image.split(filtered_image_postfix)[0]+".jpg"+segment_folder_postfix+'\\'+postfix+'.jpg')   


def segment_image(filter_img,form_type,page_numbers):
    #print(form_type)
    segmented_img_path=''
    if form_type==0:
        name_cord,dob_cord,ssn_cord=form0_segmentation_cordinates[0][0][2],form0_segmentation_cordinates[0][1][2],form0_segmentation_cordinates[0][2][2]
        for each_form in filter_img:
            crop_image(each_form,name_cord[0],name_cord[1],name_cord[2],name_cord[3],"name")
            crop_image(each_form,ssn_cord[0],ssn_cord[1],ssn_cord[2],ssn_cord[3],"ssn")
            crop_image(each_form,dob_cord[0],dob_cord[1],dob_cord[2],dob_cord[3],"dob")
        segmented_img_path=glob.glob(filter_img[0].split(filtered_image_postfix)[0]+".jpg"+segment_folder_postfix+'\\*')
        return segmented_img_path
    
    elif form_type == 1:
        name_cord,dob_cord,ssn_cord,eid_cord,gid_cord=form1_segmentation_cordinates[0][0][2],form1_segmentation_cordinates[0][1][2],form1_segmentation_cordinates[0][2][2],form1_segmentation_cordinates[0][3][2],form1_segmentation_cordinates[0][4][2]
        for each_form in filter_img:
            crop_image(each_form,name_cord[0],name_cord[1],name_cord[2],name_cord[3],"name")
            crop_image(each_form,ssn_cord[0],ssn_cord[1],ssn_cord[2],ssn_cord[3],"ssn")
            crop_image(each_form,dob_cord[0],dob_cord[1],dob_cord[2],dob_cord[3],"dob")
            crop_image(each_form,eid_cord[0],eid_cord[1],eid_cord[2],eid_cord[3],"eid")
            crop_image(each_form,gid_cord[0],gid_cord[1],gid_cord[2],gid_cord[3],"gid")
        segmented_img_path=glob.glob(filter_img[0].split(filtered_image_postfix)[0]+".jpg"+segment_folder_postfix+'\\*')
        return segmented_img_path
    return segmented_img_path    


def vision_api(segmented_img_path):
    from utils import Service, encode_image
    access_token="AIzaSyDKZ30jKSLRR-nNvMLZpWNS4qcjCrWTJiU"
    service = Service('vision', 'v1', access_token=access_token)
    
    for photo_file in segmented_img_path:
        with open(photo_file, 'rb') as image:
            base64_image = encode_image(image)
            body = {
                'requests': [{
                    'image': {
                        'content': base64_image,
                    },
                    'features': [{
                        'type': 'DOCUMENT_TEXT_DETECTION',
                        'maxResults': 1,
                    }]
    
                }]
            }
            response = service.execute(body=body)
            #print(response)
            text = response['responses'][0]['textAnnotations'][0]['description']
            print('Found text: {}'.format(text))


#SS def main():
def get_image_path(input_1):
    #file_path_input='C:\\Users\\rahuln\\Documents\\UNUM\\TestRun\\FORM0.pdf' # Form 0 Path
    
    #SS file_path_input='C:\\Users\\rahuln\\Documents\\UNUM\\TestRun\\FORM1.pdf' # Form 1 Path
    file_path_input=input_1['file_path']
    #SS seg_flag_input=True
    seg_flag_input=input_1['seg_flag']
    
    #SS filter_map_input='color,lines,noise'
    filter_map_input= input_1['filter_list']
    
    #SS form_type_input=1 #eg :- 0 or 1
    form_type_input = input_1['form_type']
    
    #SS page_numbers_input=[0] # list of pages to apply filter eg:- [0,1]
    page_numbers_input= input_1['page']
    
    
    setParameters(file_path_input,seg_flag_input,filter_map_input,form_type_input,page_numbers_input)
    create_dir(file_path,file_name)
    filter_img=apply_filters(file_path,file_name,filter_map,form_type,page_numbers)
    return filter_img #SS
    #SS segmented_img_path=segment_image(filter_img,form_type,page_numbers)
    #print(segmented_img_path)
    #SS vision_api(segmented_img_path)

def get_image_text(input_2):
    
    file_path_input=input_2['file_path']
    seg_flag_input=input_2['seg_flag']
    filter_map_input= input_2['filter_list']
    form_type_input = input_2['form_type']
    page_numbers_input= input_2['page']
    setParameters(file_path_input,seg_flag_input,filter_map_input,form_type_input,page_numbers_input)
    create_dir(file_path,file_name)
    filter_img=apply_filters(file_path,file_name,filter_map,form_type,page_numbers)
    segmented_img_path=segment_image(filter_img,form_type,page_numbers)
    print(segmented_img_path)
    vision_api(segmented_img_path)
    
    #SS segmented_img_path=segment_image(filter_img,form_type,page_numbers)
    #print(segmented_img_path)
    #SS vision_api(segmented_img_path)
    #SS if __name__ == '__main__':
    #SS    main()
    