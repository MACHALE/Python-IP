# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 15:13:44 2018

@author: srinivass
"""

#Import Libraries

from flask import Flask, abort, jsonify, request
from PDF_Process_v2 import get_image_path,get_image_text


app = Flask(__name__)

@app.route('/pdf_to_img', methods=['POST'])
def pdf_to_img():
   input_1 = request.get_json(force=True)
   
   # Uncomment the below 2 lines after setting up everything and when you are
   # ready for Integration testing
   
   #img_path= get_image_path(input_1)
   #return jsonify(results=img_path)
   
   #Dummy output for Unit testing  
   return jsonify(results=input_1)
    
@app.route('/read_pdf',methods=['POST'])
def read_pdf():
    input_2 = request.get_json(force=True)
    
    # Uncomment the below 2 lines after setting up everything and when you are
    # ready for Integration testing
   
    #img_text = get_image_text(input_2)
    #return jsonify(results=img_text)
    #Dummy output for Unit testing  
    return jsonify(results=input_2)
    
    
    
    
    